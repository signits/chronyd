chronyd
=========

The ntpd module installs the package chrony for RHEL8, and enables the service according to the parameters provided

Requirements
------------

None

Role Variables
--------------

enable_chronyd: true - [controls if the module will run at all]

chronyd_servers: ["0.pl.pool.ntp.org", "1.pl.pool.ntp.org"] - [chronyd servers list]

chronyd_key: "0 xyzzy" - [chronyd key list]

chronyd_service_name: "chronyd" - [name of the service]

chronyd_state: "started" - [whether the service should run]

chronyd_package_name: "chrony" - [name of the package to install]

chronyd_package_ensure: "present" - [state of the package]

chronyd_daemon_extra_opts: "" - [extra options to provide to the daemon]

Dependencies
------------

None

License
-------

MOT

Author Information
------------------

signits@acme.dev
